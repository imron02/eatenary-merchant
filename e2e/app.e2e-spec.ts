import { MerchantPage } from './app.po';

describe('merchant App', function() {
  let page: MerchantPage;

  beforeEach(() => {
    page = new MerchantPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
