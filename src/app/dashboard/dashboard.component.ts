import { Component, Inject }  from '@angular/core';
import { Ng2BootstrapConfig, Ng2BootstrapTheme } from 'ng2-bootstrap';

import { APP_TOKEN, Config } from '../config';

let w: any = window;

if (w && w.__theme === 'bs4') {
    Ng2BootstrapConfig.theme = Ng2BootstrapTheme.BS4;
}

@Component({
    selector    : 'app-dashboard',
    templateUrl : './dashboard.component.html'
})

export class DashboardComponent {
    title: string;

    constructor(@Inject(APP_TOKEN) config: Config) {
        this.title = config.title;
    }
}
