import { AbstractControl, ValidatorFn } from '@angular/forms';

export function passwordConfirmValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const confirm_password  = control.value;
        const parent_control    = control.root.value;

        if (parent_control) {
            if (parent_control.password !== confirm_password) {
                return { 'passwordConfirm': false };
            }
        }

        return null;
    };
}
