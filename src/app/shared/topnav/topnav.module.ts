import { NgModule }         from '@angular/core';
import { DropdownModule }   from 'ng2-bootstrap/ng2-bootstrap';

import { TopnavComponent }  from './topnav.component';

@NgModule({
    imports     : [ DropdownModule ],
    declarations: [ TopnavComponent ],
    exports     : [ TopnavComponent ]
})

export class TopnavModule { }
