import { Component }    from '@angular/core';

@Component({
    selector    : 'app-sidebar',
    templateUrl : './sidebar.component.html',
    styleUrls   : ['./styles.scss']
})

export class SidebarComponent { }
