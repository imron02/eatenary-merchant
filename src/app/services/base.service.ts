import { Response }     from '@angular/http';
import { Observable }   from 'rxjs/Observable';

export class BaseService {
    protected extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    protected handleError(error: any) {
        let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';

        console.log(errMsg);
        return Observable.throw(errMsg);
    }
}
