import { Injectable, Inject }   from '@angular/core';
import { Http }                 from '@angular/http';

import { Observable }           from 'rxjs/Observable';
import { City }                 from './city';
import { APP_TOKEN, Config }    from '../config';
import { BaseService }          from './base.service';

@Injectable()
export class CityService extends BaseService {
    private api_url: string;

    constructor(private http: Http, @Inject(APP_TOKEN) config: Config) {
        super();
        this.api_url = config.api;
    }

    getCities(province: string): Observable<City[]> {
        return this.http.get(`${this.api_url}/city?province_id=${province}`)
                .map(this.extractData)
                .catch(this.handleError);
    }
}
