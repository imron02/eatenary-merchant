import { Injectable, Inject }   from '@angular/core';
import { Http }                 from '@angular/http';

import { Observable }           from 'rxjs/Observable';
import { Province }             from './province';
import { APP_TOKEN, Config }    from '../config';
import { BaseService }          from './base.service';

@Injectable()
export class ProvinceService extends BaseService {
    private api_url: string;

    constructor(private http: Http, @Inject(APP_TOKEN) config: Config) {
        super();
        this.api_url = config.api;
    }

    getProvinces(country: string): Observable<Province[]> {
        return this.http.get(`${this.api_url}/province?country_id=${country}`)
                .map(this.extractData)
                .catch(this.handleError);
    }
}
