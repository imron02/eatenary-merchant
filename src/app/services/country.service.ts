import { Injectable, Inject }   from '@angular/core';
import { Http }                 from '@angular/http';

import { Observable }           from 'rxjs/Observable';
import { Country }              from './country';
import { APP_TOKEN, Config }    from '../config';
import { BaseService }          from './base.service';

@Injectable()
export class CountryService extends BaseService {
    private api_url: string;

    constructor(private http: Http, @Inject(APP_TOKEN) config: Config) {
        super();
        this.api_url = config.api;
    }

    getCountries(): Observable<Country[]> {
        return this.http.get(`${this.api_url}/country`)
                .map(this.extractData)
                .catch(this.handleError);
    }
}
