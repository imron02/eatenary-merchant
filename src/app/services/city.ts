export interface City {
    _id: string;
    name: string;
    province_id: string;
    created_at: string;
    updated_at: string;
}
