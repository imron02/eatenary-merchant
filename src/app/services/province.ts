export class Province {
    _id: string;
    name: string;
    country_id: string;
    created_at: string;
    updated_at: string;
}
