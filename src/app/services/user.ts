export interface User {
    _id: string;
    email: string;
    first_name: string;
    last_name: string;
    password: string;
    retype_password: string;
    birth_place: string;
    birth_date: string;
    gender: number;
    agreement: boolean;
    address?: string;
    country?: string;
    province?: string;
    city?: string;
    postal_code?: string;
    handphone?: string;
    telephone?: string;
}
