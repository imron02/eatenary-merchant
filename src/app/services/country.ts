export class Country {
    _id: string;
    name: string;
    created_at: string;
    updated_at: string;
}
