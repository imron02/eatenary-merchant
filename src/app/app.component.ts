import { Component, DoCheck }    from '@angular/core';
import { Location }              from '@angular/common';

import './services/rxjs-operators';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})

export class AppComponent implements DoCheck {
    title: string = 'Eatenary';
    dashboard: boolean = false;

    constructor(private location: Location) { }

    ngDoCheck(): void {
        if (this.location.path() !== '/login' && this.location.path() !== '/register') {
            this.dashboard = true;
        } else {
            this.dashboard = false;
        }
    }
}
