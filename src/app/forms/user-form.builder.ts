import { Injectable }                          from '@angular/core';
import { FormBuilder, FormGroup, Validators }  from '@angular/forms';

import { passwordConfirmValidator, emailValidator } from '../shared';
import * as _ from 'lodash';

@Injectable()
export class UserFormBuilder {
    /**
     * User form error field
     * 
     * @var Object
     */
    field: Object = {
        'email'                 : '',
        'first_name'            : '',
        'last_name'             : '',
        'password'              : '',
        'password_confirmation' : '',
        'birth_place'           : '',
        'birth_date'            : '',
        'gender'                : ''
    };

    /**
     * Custom message validaton
     * 
     * @var Object
     */
    validation_message = {
        'email': {
            'required'  : 'Email is required.',
            'email'     : 'Email is invalid.'
        },
        'first_name': {
            'required'  : 'First name is required.'
        },
        'last_name': {
            'required'  : 'Last name is required.'
        },
        'password': {
            'required'  : 'Password is required.',
            'minlength' : 'Password must be at least 6 characters long.'
        },
        'password_confirmation': {
            'required'       : 'Password confirmation is required.',
            'minlength'      : 'Password must be at least 6 characters long.',
            'passwordConfirm': 'Password do not match.'
        },
        'birth_place': {
            'required': 'Birth place is required.'
        },
        'birth_date': {
            'required': 'Birth date is required.'
        },
        'gender': {
            'required': 'Gender is required.'
        }
    };

    /**
     * Create a new builder instance.
     *
     * @param  @angular\forms\FormBuilder  fb
     * @return void
     */
    constructor(private fb: FormBuilder) { }

    /**
     * User form group
     * 
     * @return void
     */
    form(): FormGroup {
        return this.fb.group({
            email                   : [null, [
                    Validators.required,
                    emailValidator(),
                ]
            ],
            first_name              : [null, Validators.required],
            last_name               : [null, Validators.required],
            password                : [null, [Validators.required, Validators.minLength(6)]],
            password_confirmation   : [null, [
                    Validators.required,
                    Validators.minLength(6),
                    passwordConfirmValidator(),
                ]
            ],
            birth_place             : [null, Validators.required],
            birth_date              : [null, Validators.required],
            gender                  : [null, Validators.required],
            address                 : '',
            country_id              : '',
            province_id             : this.fb.control({ value: null, disabled: true }),
            city_id                 : this.fb.control({ value: null, disabled: true }),
            postal_code             : '',
            handphone               : '',
            telephone               : '',
            agreement               : [null, Validators.required]
        });
    }

    /**
     * Form validation error message
     * 
     * @param  @angular\forms\FormGroup  fg
     * @param  any  data is optional
     * @return void
     */
    formError(fg: FormGroup, data?: any) {
        if (!fg) { return; }
        const form     = fg;
        const messages = this.validation_message;
        const field    = this.field;

        _.forEach(field, function(value, key) {
            // clear previous error message (if any)
            field[key] = '';
            const control = form.get(key);

            if (control && control.dirty && !control.valid) {
                _.forEach(control.errors, function(value2, key2) {
                    field[key] += messages[key][key2] + ' ';
                });
            }
        });
    }
}
