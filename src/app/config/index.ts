/**
 * This barrel file provides the exports for the shared resources (config).
 * 
 */
import { OpaqueToken } from '@angular/core';

export let APP_TOKEN = new OpaqueToken('app.config');
export * from './config';
export * from './app.config';
