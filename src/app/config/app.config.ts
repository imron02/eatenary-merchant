import { Config } from './config';

export const APP_CONFIG: Config = {
    /** 
     * Application Name
     * 
     * This value is the name of your application. This value is used when the
     * framework needs to place the application's name in a notification or
     * any other location as required by the application or its packages.
     *  
     */
    title: 'Eatenary',

    /**
     * API URL. This URL is used to access api url
     *  
     */
    api: 'http://localhost:1337'
};
