import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent }     from './login/login.component';
import { RegisterComponent }  from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const appRoutes: Routes = [
    {
        path        : '',
        redirectTo  : '/login',
        pathMatch   : 'full'
    },
    {
        path        : 'login',
        component   : LoginComponent
    },
    {
        path        : 'register',
        component   : RegisterComponent
    },
    {
        path        : 'dashboard',
        component   : DashboardComponent
    },
    {
        path        : '**',
        redirectTo  : '/login',
        pathMatch   : 'full'
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
