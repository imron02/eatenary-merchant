import { BrowserModule }                      from '@angular/platform-browser';
import { NgModule }                           from '@angular/core';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HttpModule }                         from '@angular/http';

import { AppComponent }          from './app.component';
import { LoginComponent }        from './login/login.component';
import { RegisterComponent }     from './register/register.component';
import { TopnavModule }          from './shared';
import { SidebarModule }         from './shared';
import { DashboardComponent }    from './dashboard/dashboard.component';
import { routing }               from './app.routing';
import { APP_TOKEN, APP_CONFIG } from './config';
import { CapitalizePipe }        from './shared';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        DashboardComponent,
        CapitalizePipe,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        TopnavModule,
        SidebarModule,
        routing,
    ],
    providers: [
        { provide: APP_TOKEN, useValue: APP_CONFIG }
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
