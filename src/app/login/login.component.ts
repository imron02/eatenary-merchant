import { Component, Inject }  from '@angular/core';
import { APP_TOKEN, Config }  from '../config';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})

export class LoginComponent {
    title: string;

    constructor(@Inject(APP_TOKEN) config: Config) {
        this.title = config.title;
    }
}
