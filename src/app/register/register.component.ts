import { Component, OnInit, Inject }    from '@angular/core';
import { FormGroup }                    from '@angular/forms';

import {
    Country,
    CountryService,
    Province,
    ProvinceService,
    City,
    CityService
} from '../services';
import { APP_TOKEN, Config }    from '../config';
import { UserFormBuilder }      from '../forms';

@Component({
    selector    : 'app-register',
    templateUrl : './register.component.html',
    providers   : [
        CountryService,
        ProvinceService,
        CityService,
        UserFormBuilder
    ]
})

export class RegisterComponent implements OnInit {
    /**
     * Set title view component
     * 
     * @var string
     */
    title: string;

    /**
     * Set loader for province
     * 
     * @var string
     */
    province_loading: string = 'hidden';

    /**
     * Set loader for city
     * 
     * @var string
     */
    city_loading: string = 'hidden';

    /**
     * Set variable for get message error
     * 
     * @var string
     */
    errorMessage: string;

    /**
     * The country model instance
     * 
     * @var \app\services\Country
     */
    countries: Country[];

    /**
     * The province model instance
     * 
     * @var \app\services\Province
     */
    provinces: Province[];

    /**
     * The city model instance
     * 
     * @var \app\services\City
     */
    cities: City[];

    /**
     * The form group instance
     * 
     * @var @angular\forms\FormGroup
     */
    form: FormGroup;

    /**
     * Accessing form error from form builder
     * 
     * @var Object
     */
    formErrors: Object;

    /**
     * Create a new component instance.
     *
     * @param  \app\services\CountryService  countryService
     * @param  \app\services\ProvinceService  provinceService
     * @param  \app\services\CityService  cityService
     * @param  \node_modules\@angular\forms  fb
     * @return void
     */
    constructor(
        private countryService: CountryService,
        private provinceService: ProvinceService,
        private cityService: CityService,
        private userFb: UserFormBuilder,
        @Inject(APP_TOKEN) config: Config
    ) {
        this.title = config.title;
    }

    /**
     * Component init
     * 
     * @return void
     */
    ngOnInit(): void {
        this.getCountries();
        this.form       = this.userFb.form();
        this.formErrors = this.userFb.field;

        this.form.valueChanges
            .subscribe(data => this.userFb.formError(this.form, data));
    }

    /**
     * Display a listing of the country.
     *
     * @return void
     */
    getCountries(): void {
        this.countryService.getCountries()
            .subscribe(
                countries   => this.countries = countries,
                error       => this.errorMessage = <any>Error
            );
    }

    /**
     * Display a listing of the province.
     *
     * @return void
     */
    getProvinces(country: string): void {
        this.provinceService.getProvinces(country)
            .subscribe(
                (provinces) => {
                    this.provinces          = provinces;
                    this.province_loading   = 'hidden';

                    // remove disabled
                    this.form.get('province_id').enable();
                },
                error => this.errorMessage = <any>Error
            );
    }

    /**
     * Display a listing of the city.
     *
     * @return void
     */
    getCities(province: string): void {
        this.cityService.getCities(province)
            .subscribe(
                (cities) => {
                    this.cities         = cities;
                    this.city_loading   = 'hidden';

                    // remove disabled
                    this.form.get('city_id').enable();
                },
                error => this.errorMessage = <any>Error
            );
    }

    /**
     * Get listing province by country
     *
     * @param  string  country
     * @return void
     */
    countryChanged(country: string): void {
        this.province_loading = 'visible';
        this.getProvinces(country);
    }

    /**
     * Get listing city by province
     *
     * @param  string  province
     * @return void
     */
    provinceChanged(province: string): void {
        this.city_loading = 'visible';
        this.getCities(province);
    }
}
